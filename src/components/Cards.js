import React, { Component } from 'react';
import {Image, StyleSheet, View as NativeView} from 'react-native';
import { H2, H3, Header, View, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon } from 'native-base';

const cards = [
  {
    user: {
      name: 'Серега Батькович',
      city: 'Москва',
      years: '29',
      image: null,
    },
    event: {
      title: 'Идем смотреть игру Спартака',
      date: '20 октября',
      time: '20:00',
      location: {
        title: 'Открытие арена'
      },
      people: {
        count: 3
      },
      likes: {
        count: 12
      }
    },
  },
  {
    user: {
      name: 'Макчим Петрович',
      city: 'Москва',
      years: '29',
      ava: {
        img: '',
      }
    },
    event: {
      title: 'Идем смотреть игру Спартака',
      date: '20 октября',
      time: '20:00',
      location: {
        title: 'Открытие арена'
      },
      people: {
        count: 3
      },
      likes: {
        count: 12
      }
    },
  },
];

export default class Cards extends Component {
  renderItem = ({ user: { name, city, years, image }, event: { title, date, time, location: { title: locationTitle }, people: { count: peopleCount } }}) => {
    return (
      <Card style={{ elevation: 3 }}>
        <CardItem bordered cardBody>
          <Left style={styles.person}>
            {
              image ? <Thumbnail source={image} /> : (
                <NativeView style={styles.imgContainer}>
                  <Icon name="ios-person" color="#fff" style={{ color: '#fff' }} />
                </NativeView>
              )
            }
            <Body>
              <Text>{name}</Text>
              <Text note>{city}, {`${years} лет`}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem bordered cardBody>
          <Body style={styles.body}>
            <View style={styles.rowItem}>
              <H2>{title}</H2>
            </View>
            <View style={styles.rowItem}>
              <Text style={{ marginBottom: 5 }}>Когда</Text>
              <H3>{`${date} в ${time}`}</H3>
            </View>
            <View style={styles.rowItem}>
              <Text style={{ marginBottom: 5 }}>Где</Text>
              <H3>{locationTitle}</H3>
            </View>
            <View style={styles.rowItem}>
              <Text style={{ marginBottom: 5 }}>Кол-во участников</Text>
              <H3>{peopleCount}</H3>
            </View>
          </Body>
        </CardItem>
      </Card>
    )
  };

  render() {
    return (
      <DeckSwiper
        dataSource={cards}
        renderItem={this.renderItem}
        style={styles.container}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  person: {
    padding: 0,
    margin: 0
  },
  body: {
    padding: 20,
  },
  imgContainer: {
    backgroundColor: '#273BF1',
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 25,
    paddingRight: 25,
  },
  rowItem: {
    paddingBottom: 20
  }
});