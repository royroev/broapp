import Auth0 from 'react-native-auth0';
import Config from "react-native-config";
import DeviceInfo from "react-native-device-info";

const auth0 = new Auth0({
  domain: Config.AUTH0_DOMAIN,
  clientId: Config.AUTH0_CLIENT_ID
});

import { AsyncStorage } from "react-native";

export const USER_KEY = "auth-demo-key";

export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");

export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const login = () => {
  auth0.webAuth
    .authorize({
      scope: Config.AUTHO_SCOPE,
      audience: Config.AUTH0_AUDIENCE,
      // device: DeviceInfo.getUniqueID(),
      prompt: "login"
    })
    .then(res => {
      auth0.auth
        .userInfo({ token: res.accessToken })
        .then(data => {
          this.gotoAccount(data);
        })
        .catch(err => {
          console.log("err: ");
          console.log(JSON.stringify(err));
        });

      SInfo.setItem("accessToken", res.accessToken, {});
      SInfo.setItem("refreshToken", res.refreshToken, {});
    })
    .catch(error => {
      console.log("error occurrdzz");
      console.log(error);
    });
};

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(USER_KEY)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export default class Auth {
  auth0 = new {

  }
}
