/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { Platform, StyleSheet, View, StatusBar } from 'react-native';
import { StyleProvider } from 'native-base';
import { reduxifyNavigator } from 'react-navigation-redux-helpers';
import { PersistGate } from 'redux-persist/integration/react';

import configureStore from './redux/store';

import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import AppNavigator from './navigation/AppNavigator';

const AppNavigatorWithRedux = reduxifyNavigator(AppNavigator, "root");

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const { store, persist } = configureStore();
const mapStateToProps = (state) => ({
  state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(AppNavigatorWithRedux);

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persist}>
          <StyleProvider style={getTheme(material)}>
            <View style={styles.container}>
              {Platform.OS === 'ios' && (
                <StatusBar backgroundColor="blue" barStyle="default" />
              )}
              <AppWithNavigationState />
            </View>
          </StyleProvider>
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
