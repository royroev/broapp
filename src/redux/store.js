import thunkMiddleware from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createReactNavigationReduxMiddleware, createNavigationReducer } from 'react-navigation-redux-helpers';
import { composeWithDevTools } from 'remote-redux-devtools';
import { persistStore, persistReducer } from 'redux-persist'
import { AsyncStorage } from 'react-native';

import myEvents from './reducers/myEvents';
import auth from './reducers/auth';
import AppNavigator from '../navigation/AppNavigator';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const navReducer = createNavigationReducer(AppNavigator);

const navMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const rootReducer = combineReducers({
  nav: navReducer,
  myEvents,
  auth,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureStore = () => {
  const store = createStore(persistedReducer, composeWithDevTools(
    applyMiddleware(thunkMiddleware, navMiddleware)
  ));
  const persist = persistStore(store);

  return { store, persist }
};

export default configureStore;
