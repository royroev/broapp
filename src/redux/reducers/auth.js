import { NavigationActions } from "react-navigation";

import { onSignIn, onSignOut } from '../../auth';

export const SET_USER = 'SET_USER';
export const REMOVE_USER = 'REMOVE_USER';

const initialState = {
  user: null,
};

export default function myEvents(state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return { ...state, user: action.payload.user };

    case REMOVE_USER:
      return { ...state, user: null };

    default:
      return state;
  }
}

export const setUser = (user) => {
  return {
    type: SET_USER,
    payload: {
      user,
    }
  };
};

export const removeUser = () => ({ type: REMOVE_USER });

export const registration = () => (dispatch) => {
  onSignIn().then(() => {
    dispatch(setUser({ name: "Михаил Петрович"}));

    dispatch(NavigationActions.navigate({ routeName: 'Profile' }));
  })
};

export const logout = () => (dispatch) => {
  onSignOut().then(() => {
    dispatch(removeUser());

    dispatch(NavigationActions.navigate({ routeName: 'Registration' }));
  })
};
