export const MY_EVENT_OPEN_FORM = 'MY_EVENT_OPEN_FORM';

const initialState = {
  openForm: false,
};

export default function myEvents(state = initialState, action) {
  switch (action.type) {
    case MY_EVENT_OPEN_FORM: {
      return { ...state, openForm: true }
    }

    default:
      return state;
  }
}

export const openForm = () => {
  return { type: MY_EVENT_OPEN_FORM };
};
