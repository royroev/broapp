import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export default class MessagesScreen extends React.Component {
  static navigationOptions = {
    title: 'Сообщения',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>У вас пока нет сообщений</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
