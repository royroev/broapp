import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button, Container, Content, Icon } from 'native-base';

export default class MyEventsScreen extends React.Component {
  static navigationOptions = {
    title: 'Мои события',
  };

  openForm = () => {
    this.props.navigation.navigate('AddNewEvent');
  };

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <View>
            <Text>У вас пока нет событий</Text>
            <Button iconRight full large onPress={this.openForm}>
              <Icon name='add-circle' />
              <Text>Создать новое событиие</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // paddingTop: 15,
    // backgroundColor: '#fff',
  },
});
