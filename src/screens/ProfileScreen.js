import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import { Button, Container, Content, Icon, Header } from 'native-base';

import { logout } from '../redux/reducers/auth';

class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Профиль',
  };

  componentDidMount() {
    if (!this.props.auth.user) {
      this.props.navigation.navigate('Registration');
    }
  }

  render() {
    if (!this.props.auth.user) {
      return <Text>Загрузка...</Text>;
    }

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="default" />
        <Header>
          <Text>Профиль</Text>
        </Header>
        <Content>
          <View>
            <Text>добро пожаловать {this.props.auth.user.name}</Text>
            <Button
              title="LOGOUT"
              onPress={this.props.logout}
            >
              <Text>Выйти</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 15,
    // backgroundColor: '#fff',
  },
});

const mapStateToProps = ({ auth }) => ({
  auth,
});

const mapDispatchToProps = {
  logout
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
