import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button, Container, Content, Form, Icon, Input, Item, Label } from "native-base";

export default class MessagesScreen extends React.Component {
  static navigationOptions = {
    title: 'Новое событие',
  };

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Место проведениия</Label>
              <Input />
              <Icon active name='pin' />
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
