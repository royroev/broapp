import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text } from 'react-native';
import { Button, Container, Content, Form, Item, Input, Label } from 'native-base';

import { registration } from '../redux/reducers/auth';
import { login } from '../auth';

class RegistrationScreen extends React.Component {
  static navigationOptions = {
    title: 'Регистрация',
  };

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <View style={styles.socials}>
            <Text style={styles.whiteText}>Bro, нам нужны данные о тебе, чтобы другие bro видели кто создал событие, ты можешь дать доступ к одной из своей социальной сети или указать данные самостоятельно.</Text>
            <Button light onPress={login}>
              <Text>Facebook</Text>
            </Button>
            <Button light>
              <Text>ВКонтакте</Text>
            </Button>
          </View>
          <Form>
            <Item floatingLabel>
              <Label>Как тебя зовут</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Сколько тебе лет</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Номер телефона</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Пару строк о себе</Label>
              <Input />
            </Item>
            <Button
              title="SIGN IN"
              onPress={this.props.registration}
            >
              <Text>Зарегистрироваться</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 15,
    // backgroundColor: '#fff',
  },
  socials: {
    backgroundColor: '#273bf1',
    paddingTop: 32,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 32
  },
  whiteText: {
    color: '#fff'
  }
});

const mapStateToProps = ({ auth }) => ({
  auth,
});

const mapDispatchToProps = {
  registration
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen);
